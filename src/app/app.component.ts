import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Storage} from '@ionic/storage';
import { HomePage } from '../pages/home/home';
import { LangPage } from '../pages/lang/lang';
import { ProgramPage } from '../pages/program/program';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = ProgramPage;
  lang:any;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,private storage :Storage) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });

  //   storage.get('lang').then((val) => {
  //   console.log('Your lang', val);

  //   this.lang = val;
  //   console.log(this.lang);
  //   if(this.lang == 'eng' || this.lang == 'tel'){
  //     this.rootPage = HomePage;
  //   }else{
  //     this.rootPage = LangPage;
  //   }
  // });


  // }
}
}

