import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WatchPage } from './watch';
// import { VideoPipe} from '../../pipes/video/video';
import { PipesModule } from '../../pipes/pipes.module';


@NgModule({
  declarations: [
    WatchPage,
    // VideoPipe,

  ],
  // exports: [VideoPipe],
  imports: [
  	PipesModule,
    IonicPageModule.forChild(WatchPage),
  ],
})
export class WatchPageModule {}
