import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

import { Http } from '@angular/http';
import        'rxjs/add/operator/map';



@IonicPage()
@Component({
  selector: 'page-video',
  templateUrl: 'video.html',
})
export class VideoPage {

  	channelId:string='UCULLmfWqhMOeiWEV6hkMSCg';
    maxRes:string='8';
    googleToken:string='AIzaSyC1Q9V_RvWZ99Bzkf44eZoYhjseK_UINP4';
    search:string='Video';
    posts:any=[];
    postData:any;



  constructor(public navCtrl: NavController, public navParams: NavParams, public http :Http) {




   let url="https://www.googleapis.com/youtube/v3/search?part=id,snippet&channelId="
    +this.channelId+"&q="+this.search+"&type=video&order=date&maxResults="+this.maxRes+"&key="+this.googleToken;
    
    this.http.get(url).map(res=>res.json()).subscribe(data=>{
      
      if(data!=undefined && data!=null 
      && data.items!=undefined && data.items!=null) {
      console.log(data.items);
      this.posts=this.posts.concat(data.items);
      console.log(this.posts);
      }
    });
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideoPage');
  }

}
